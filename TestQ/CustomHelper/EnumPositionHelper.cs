﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestQ.Model.AppConstants;

namespace TestQ.CustomHelper
{
    public static class EnumPositionHelper
    {
        public static IHtmlString GetPosition(int id)
        {
            var position = (Constants.Positions) id;
            string lableStr = $"{position}";
            return new HtmlString(lableStr);
        }
    }
}