﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestQ.Model.Models;


namespace TestQ.ViewModel
{
    public class AddEditEmployeeViewModel
    {
        public Employee Employee { get; set; }
        public IEnumerable<SelectListItem> SelectListCompanies { get; set; }     
    }
}