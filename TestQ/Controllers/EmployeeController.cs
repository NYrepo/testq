﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TestQ.Model.Models;
using TestQ.Service.Interfaces;
using TestQ.Service.Services;
using TestQ.ViewModel;

namespace TestQ.Controllers
{
    public class EmployeeController : Controller
    {
        private IService<Employee> employeeService;
        private IService<Company> companyService;

        public EmployeeController()
        {
            employeeService = EmployeeService.Instance();
            companyService = CompanyService.Instance();
        }
        public ActionResult GetEmployeeList()
        {
            return View(employeeService.GetAll().AsEnumerable());
        }
        public ActionResult Delete(int id)
        {
            employeeService.Delete(id);
            return RedirectToAction("GetEmployeeList");
        }
        public ActionResult Create()
        {
            return View(new AddEditEmployeeViewModel
            {
                Employee = new Employee
                {
                    HiringDate = DateTime.Now,
                },
                SelectListCompanies = GetCompanyNames()
            });
        }
        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    employeeService.Add(employee);
                    return RedirectToAction("GetEmployeeList");
                }
                catch
                {
                    return View(new AddEditEmployeeViewModel
                    {
                        Employee = employee,
                        SelectListCompanies = GetCompanyNames()
                    });
                }
            }
            else
            {
                return View(new AddEditEmployeeViewModel
                {
                    Employee = employee,
                    SelectListCompanies = GetCompanyNames()
                });
            }
        }
        public ActionResult Edit(int id)
        {
            var employee = employeeService.GetById(id);
            
            return View(new AddEditEmployeeViewModel
            {
                Employee = employee,
                SelectListCompanies = GetCompanyNames(employee.CompanyId)
            });
        }
        [HttpPost]
        public ActionResult Edit(Employee employee)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    employeeService.Update(employee);
                    return RedirectToAction("GetEmployeeList");
                }
                catch(Exception ex)
                {
                    return View(new AddEditEmployeeViewModel
                    {
                        Employee = employee,
                        SelectListCompanies = GetCompanyNames(employee.CompanyId)
                    });
                }
            }
            else
            {
                return View(new AddEditEmployeeViewModel
                {
                    Employee = employee,
                    SelectListCompanies = GetCompanyNames(employee.CompanyId)
                });
            }
        }
        [NonAction]
        private List<SelectListItem> GetCompanyNames(int companyId = -1)
        {
            var companiesNameSelectListItems = new List<SelectListItem>();

            foreach (var company in companyService.GetAll())
            {
                var selectedItem = new SelectListItem()
                {
                    Text = company.Name,
                    Value = company.Id.ToString()
                };
                if (companyId!=1 && companyId == company.Id)
                {
                    selectedItem.Selected = true;
                }
                companiesNameSelectListItems.Add(selectedItem);
            }
            return companiesNameSelectListItems;
        }
    }
}