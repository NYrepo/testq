﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestQ.Model;
using TestQ.Model.Models;
using TestQ.Service.Interfaces;
using TestQ.Service.Services;

namespace TestQ.Controllers
{
    public class HomeController : Controller
    {
        private IService<Employee> employeesService;
        private IService<Company> companyService;
        public HomeController()
        {
            employeesService = EmployeeService.Instance();
            companyService = CompanyService.Instance();
        }
        // GET: Home
        public ActionResult Index()
        {
            //var repo = new CompanyRepository();
            //repo.Add(new Company
            //{
            //    Id = 11,
            //    OrgForm = "Zao",
            //    Size = 1500,
            //    Name = "Coca-Cola"
            //});
            //var resultList = repo.GetAll();
            //repo.Update(new Company
            //{
            //    Id = 11,
            //    OrgForm = "OAO",
            //    Size = 2500,
            //    Name = "Coca-Cola-Corp"
            //});
            //resultList = repo.GetAll();
            //repo.Delete(10);
            //resultList = repo.GetAll();
            //var repoEmployee = new EmployeeRepository();
            //var employeeList = repoEmployee.GetAll();
            //repoEmployee.Add(new Employee
            //{
            //    Id = 56,
            //    Name = "dasdsad",
            //    Company = new Company
            //    {
            //        Id = 11
            //    },
            //    Position = "dsadasd",
            //    Surname = "dasdwq",
            //    SecondName = "dsadqweq",
            //    HiringDate = Convert.ToDateTime("2017-11-21")
            //});
            //employeeList = repoEmployee.GetAll();
            //repoEmployee.Update(new Employee
            //{
            //    Id = 56,
            //    Name = "Fred",
            //    Company = new Company
            //    {
            //        Id = 11
            //    },
            //    Position = "Manager",
            //    Surname = "dasdwq",
            //    SecondName = "dsadqweq",
            //    HiringDate = Convert.ToDateTime("2017-11-21")
            //});
            //employeeList = repoEmployee.GetAll();
            //repoEmployee.Delete(56);
            //employeeList = repoEmployee.GetAll();
            //var employeeList = employeesService.GetAll();
            //var companyList = companyService.GetAll();
            //employeesService.Add(new Employee
            //{
            //    Name = "Jora",
            //    Surname = "Sparrow",
            //    SecondName = "Petrovich",
            //    Company = companyService.GetById(11),
            //    Position = "Manager",
            //    HiringDate = Convert.ToDateTime("2017-11-21")
            //});
            //var employees = employeesService.GetAll();
            return View();
        }
    }
}