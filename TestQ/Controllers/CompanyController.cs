﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestQ.Model.Models;
using TestQ.Service.Interfaces;
using TestQ.Service.Services;

namespace TestQ.Controllers
{
    public class CompanyController : Controller
    {
        private IService<Company> companyService;
        public CompanyController()
        {
            companyService = CompanyService.Instance();
        }

        public ActionResult Create()
        {
            return View(new Company());
        }
        [HttpPost]
        public ActionResult Create(Company company)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    companyService.Add(company);
                    return RedirectToAction("GetCompanyList");
                }
                catch
                {
                    return View(company);
                }
            }
            else
            {
                return View(company);
            }
        }

        public ActionResult GetCompanyList()
        {
            return View(companyService.GetAll().AsEnumerable());
        }
        public ActionResult Delete(int id)
        {
            companyService.Delete(id);
            return RedirectToAction("GetCompanyList");
        }
        public ActionResult Edit(int id)
        {
            var company = companyService.GetById(id);
            return View(company);
        }
        [HttpPost]
        public ActionResult Edit(Company company)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    companyService.Update(company);
                    return RedirectToAction("GetCompanyList");
                }
                catch
                {
                    return View(company);
                }
            }
            else return View(company);
        }
    }
}