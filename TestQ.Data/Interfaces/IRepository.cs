﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestQ.Data.Interfaces
{
    public interface IRepository<T> where T : class
    {
        T GetById(int i);
        bool Add(T obj);
        List<T> GetAll();
        bool Update(T obj);
        bool Delete(int id);
    }
}
