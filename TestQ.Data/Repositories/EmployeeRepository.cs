﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TestQ.Model.Models;

namespace TestQ.Data.Repositories
{
    public class EmployeeRepository : BaseRepository<Employee>
    {
        private static EmployeeRepository _instance;

        private EmployeeRepository()
        {

        }
        public static EmployeeRepository Instance()
        {
            return _instance ?? (_instance = new EmployeeRepository());
        }
        public override Employee GetById(int id)
        {
            Connection();
            var com = new SqlCommand("GetEmployeeById", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@EmployeeId", id);
            var da = new SqlDataAdapter(com);
            var dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            var dr = dt.Rows[0];

            var employeeResult = new Employee   
            {
                Id = Convert.ToInt32(dr["EmployeeId"]),
                Name = Convert.ToString(dr["Name"]),
                SecondName = Convert.ToString(dr["SecondName"]),
                Surname = Convert.ToString(dr["Surname"]),
                Position = Convert.ToInt32(dr["Position"]),
                HiringDate = Convert.ToDateTime(dr["HiringDate"]),
                Company = new Company
                {
                    Id = Convert.ToInt32(dr["CompanyId"]),
                    Name = Convert.ToString(dr["CompanyName"]),
                    OrgForm = Convert.ToString(dr["OrgForm"]),
                    Size = Convert.ToInt32(dr["Size"])
                },
                CompanyId = Convert.ToInt32(dr["CompanyId"])
            };
            return employeeResult;
        }
        public override bool Add(Employee obj)
        {
            Connection();
            var com = new SqlCommand("AddNewEmployee", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Name", obj.Name);
            com.Parameters.AddWithValue("@SecondName", obj.SecondName);
            com.Parameters.AddWithValue("@Surname", obj.Surname);
            com.Parameters.AddWithValue("@HiringDate", obj.HiringDate);
            com.Parameters.AddWithValue("@Position", obj.Position);
            com.Parameters.AddWithValue("@CompanyId", obj.CompanyId);

            con.Open();
            var executionIntResult = com.ExecuteNonQuery();
            con.Close();
            return executionIntResult >= 1;
        }
        public override List<Employee> GetAll()
        {
            Connection();
            var employeesList = new List<Employee>();
            var com = new SqlCommand("GetEmployees", con);
            com.CommandType = CommandType.StoredProcedure;
            var da = new SqlDataAdapter(com);
            var dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            
            foreach (DataRow dr in dt.Rows)
            {
                employeesList.Add(new Employee
                {
                    Id = Convert.ToInt32(dr["EmployeeId"]),
                    Name = Convert.ToString(dr["Name"]),
                    SecondName = Convert.ToString(dr["SecondName"]),
                    Surname = Convert.ToString(dr["Surname"]),
                    Position = Convert.ToInt32(dr["Position"]),
                    HiringDate = Convert.ToDateTime(dr["HiringDate"]),
                    Company = new Company
                    {
                        Id = Convert.ToInt32(dr["CompanyId"]),
                        Name = Convert.ToString(dr["CompanyName"]),
                        OrgForm = Convert.ToString(dr["OrgForm"]),
                        Size = Convert.ToInt32(dr["Size"])
                    },
                    CompanyId = Convert.ToInt32(dr["CompanyId"])
                });
            }
            return employeesList;
        }
        public override bool Update(Employee obj)
        {
            Connection();
            var com = new SqlCommand("UpdateEmployee", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@EmployeeId", obj.Id);
            com.Parameters.AddWithValue("@Name", obj.Name);
            com.Parameters.AddWithValue("@SecondName", obj.SecondName);
            com.Parameters.AddWithValue("@Surname", obj.Surname);
            com.Parameters.AddWithValue("@Position", obj.Position);
            com.Parameters.AddWithValue("@CompanyId", obj.CompanyId);
            com.Parameters.AddWithValue("@HiringDate", obj.HiringDate);
            con.Open();
            var executionIntResult = com.ExecuteNonQuery();
            con.Close();
            return executionIntResult >= 1;
        }
        public override bool Delete(int id)
        {
            Connection();
            var com = new SqlCommand("DeleteEmployeeById", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@EmployeeId", id);
            con.Open();
            var executionIntResult = com.ExecuteNonQuery();
            con.Close();
            return executionIntResult >= 1;
        }
    }
}
