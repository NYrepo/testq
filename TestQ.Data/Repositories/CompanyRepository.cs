﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestQ.Data.Repositories;
using TestQ.Model.Models;

namespace TestQ.Data.Repositories
{
    public class CompanyRepository : BaseRepository<Company>
    {
        private static CompanyRepository _instance;

        private CompanyRepository()
        {
            
        }
        public static CompanyRepository Instance()
        {
            return _instance ?? (_instance = new CompanyRepository());
        }
        public override bool Add(Company obj)
        {
            Connection();
            var com = new SqlCommand("AddNewCompany", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@CompanyId", obj.Id);
            com.Parameters.AddWithValue("@Name", obj.Name);
            com.Parameters.AddWithValue("@Size", obj.Size);
            com.Parameters.AddWithValue("@OrgForm", obj.OrgForm);

            con.Open();
            var executionIntResult = com.ExecuteNonQuery();
            con.Close();
            return executionIntResult >= 1;
        }
        public override List<Company> GetAll()
        {
            Connection();
            var companyList = new List<Company>();
            var com = new SqlCommand("GetCompanies", con);
            com.CommandType = CommandType.StoredProcedure;
            //SqlCommand command = new SqlCommand();
            //command.CommandText = "SELECT * FROM Companies";
            //command.Connection = con;
            var da = new SqlDataAdapter(com);
            var dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                companyList.Add(new Company
                {
                    Id = Convert.ToInt32(dr["CompanyId"]),
                    Name = Convert.ToString(dr["Name"]),
                    Size = Convert.ToInt32(dr["Size"]),
                    OrgForm = Convert.ToString(dr["OrgForm"])
                });
            }
            return companyList;
        }
        public override bool Update(Company obj)
        {
            Connection();
            var com = new SqlCommand("UpdateCompany", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@CompanyId", obj.Id);
            com.Parameters.AddWithValue("@Name", obj.Name);
            com.Parameters.AddWithValue("@Size", obj.Size);
            com.Parameters.AddWithValue("@OrgForm", obj.OrgForm);
            con.Open();
            var executionIntResult = com.ExecuteNonQuery();
            con.Close();
            return executionIntResult >= 1;
        }
        public override bool Delete(int id)
        {
            Connection();
            var com = new SqlCommand("DeleteCompanyById", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@CompanyId", id);
            con.Open();
            var executionIntResult = com.ExecuteNonQuery();
            con.Close();
            return executionIntResult >= 1;
        }
        public override Company GetById(int id)
        {
            Connection();
            var com = new SqlCommand("GetCompanyById", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@CompanyId", id);
            var da = new SqlDataAdapter(com);
            var dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            var dr = dt.Rows[0];

            var companyResult = new Company
            {
                Id = Convert.ToInt32(dr["CompanyId"]),
                Name = Convert.ToString(dr["Name"]),
                Size = Convert.ToInt32(dr["Size"]),
                OrgForm = Convert.ToString(dr["OrgForm"])
            };
            return companyResult;
        }
    }
}
