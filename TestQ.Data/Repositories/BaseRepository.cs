﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestQ.Data.Interfaces;


namespace TestQ.Data.Repositories
{
    public abstract class BaseRepository<T> : IRepository<T> where T : class
    {
        protected SqlConnection con;
        //To Handle connection related activities
        protected void Connection()
        {
            var constr = ConfigurationManager.ConnectionStrings["TestQxDataBase"].ToString();
            con = new SqlConnection(constr);

        }
        public abstract bool Add(T obj);
        public abstract List<T> GetAll();
        public abstract bool Update(T obj);
        public abstract bool Delete(int id);
        public abstract T GetById(int id);
    }
}
