﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;

namespace TestQ.Model.Models
{
    public class Employee
    {
        [Required(ErrorMessage = "Please enter the id of the employee")]
        [Display(Name = "Employee Id")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter the name of the employee")]
        [Display(Name = "Employee Name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please enter the second name of the employee")]
        [Display(Name = "Employee Second Name")]
        public string SecondName { get; set; }
        [Required(ErrorMessage = "Please enter the surname of the employee")]
        [Display(Name = "Employee Surname")]
        public string Surname { get; set; }
        [Required(ErrorMessage = "Please enter the position of the employee")]
        [Display(Name = "Employee Position")]
        public int Position { get; set; }
        [Required(ErrorMessage = "Please enter the Hiring Date")]
        [Display(Name = "Hiring Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime HiringDate { get; set; }
        public Company Company { get; set; }
        [Required(ErrorMessage = "Please choose the company")]
        [Display(Name = "Company")]
        public int CompanyId { get; set; } 
    }
}
