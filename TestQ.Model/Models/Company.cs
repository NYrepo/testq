﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TestQ.Model.Models
{
    public class Company
    {
        [Required(ErrorMessage = "Please enter the id of the company")]
        [Display(Name = "Company Id")]
        [Range(1, int.MaxValue, ErrorMessage = "Should be more than 0")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter the name of the company")]
        [Display(Name = "Company Name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please enter the size of the company")]
        [Range(1, int.MaxValue, ErrorMessage = "Should be more than 0")]
        [Display(Name = "Company Size")]
        public int Size { get; set; }
        [Required(ErrorMessage = "Please enter the organizational and legal form of the company")]
        [Display(Name = "Company Form")]
        public string OrgForm { get; set; }
    }
}
