﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestQ.Model.AppConstants
{
    public static class Constants
    {
        public enum Positions   
        {
            [Display(Name = "Developer")]
            Developer = 1,
            [Display(Name = "Tester")]
            Tester = 2,
            [Display(Name = "Analytic")]
            Analytic = 3,
            [Display(Name = "Manager")]
            Manager = 4
        }
    }
}
