﻿using System.Collections.Generic;
using TestQ.Data.Interfaces;
using TestQ.Data.Repositories;
using TestQ.Model.Models;
using TestQ.Service.Interfaces;

namespace TestQ.Service.Services
{
    public class EmployeeService : IService<Employee>
    {
        private IRepository<Employee> employeeRepo;
        private static EmployeeService _instance;

        private EmployeeService()
        {
            employeeRepo = EmployeeRepository.Instance();
        }

        public static EmployeeService Instance()
        {
            return _instance ?? (_instance = new EmployeeService());
        }
        public Employee GetById(int id)
        {
           return employeeRepo.GetById(id);
        }
        public bool Add(Employee obj)
        {
            return employeeRepo.Add(obj);
        }
        public bool Delete(int id)
        {
            return employeeRepo.Delete(id);
        }
        public List<Employee> GetAll()
        {
            return employeeRepo.GetAll();
        }
        public bool Update(Employee obj)
        {
            return employeeRepo.Update(obj);
        }
    }
}
