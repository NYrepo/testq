﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestQ.Data.Interfaces;
using TestQ.Data.Repositories;
using TestQ.Model.Models;
using TestQ.Service.Interfaces;

namespace TestQ.Service.Services
{
    public class CompanyService : IService<Company>
    {
        private IRepository<Company> companyRepo;
        private static CompanyService _instance;

        private CompanyService()
        {
            companyRepo = CompanyRepository.Instance();
        }

        public static CompanyService Instance()
        {
            return _instance ?? (_instance = new CompanyService());
        }

        public bool Add(Company obj)
        {
            return companyRepo.Add(obj);
        }

        public List<Company> GetAll()
        {
            return companyRepo.GetAll();
        }

        public bool Update(Company obj)
        {
            return companyRepo.Update(obj);
        }

        public bool Delete(int id)
        {
            return companyRepo.Delete(id);
        }

        public Company GetById(int id)
        {
            return companyRepo.GetById(id);
        }
    }
}
