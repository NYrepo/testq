﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestQ.Service.Interfaces
{
    public interface IService<T> where T : class
    {
        T GetById(int id);
        bool Add(T obj);
        List<T> GetAll();
        bool Update(T obj);
        bool Delete(int id);
    }
}
